<?php

namespace App\Traits;

use App\Models\Cycle;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait InteractsWithCycles
{
    public function scopeFromCurrentCycle(Builder $query, Cycle $cycle)
    {
        // @todo: refactor
        return $query->where('date', '>=', $cycle->start_date)
            ->where('date', '<=', $cycle->end_date);
    }

    public function scopeFromDate(Builder $query, Carbon $date)
    {
        return $query->where('date', '>=', $date);
    }
}