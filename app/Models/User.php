<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function leaveRequests()
    {
        return $this->hasMany(LeaveRequest::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function leaveDays()
    {
        return $this->hasMany(LeaveDay::class);
    }

    public function supervisors()
    {
        return $this->belongsToMany(
            User::class,
            'user_supervisor',
            'supervised_id',
            'supervisor_id'
        );
    }

    public function supervised()
    {
        return $this->belongsToMany(
            User::class,
            'user_supervisor',
            'supervisor_id',
            'supervised_id'
        );
    }

    public function cycles()
    {
        return $this->hasMany(Cycle::class);
    }

    public function getCurrentCycleAttribute()
    {
        return $this->cycles->sortByDesc('start_date')->first();
    }
}
