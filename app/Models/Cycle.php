<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Cycle extends Model
{
    public $timestamps = false;

    public function scopeCurrent(Builder $query)
    {
        return $query->latest('id');
    }
}
