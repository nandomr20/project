<?php

namespace App\Models;

use App\Traits\InteractsWithCycles;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class LeaveDay extends Model
{
    use InteractsWithCycles;

    const PERIODS = [
        'full_day',
        'morning',
        'afternoon',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function policy()
    {
        return $this->belongsTo(LeavePolicy::class);
    }

    public function leaveRequestApproval()
    {
        // @todo: make this relation to work
        return $this->hasManyThrough(LeaveRequestApproval::class, LeaveRequest::class);
    }

    public function scopeOfTypeHoliday(Builder $query)
    {
        return $query->where('policy', LeavePolicy::DEFAULT_LEAVE_POLICY_IDS['holiday']);
    }

    public function scopeOfTypeSick(Builder $query)
    {
        return $query->where('policy', LeavePolicy::DEFAULT_LEAVE_POLICY_IDS['sick']);
    }

    public function scopeOfTypeBankHoliday(Builder $query)
    {
        return $query->where('policy', LeavePolicy::DEFAULT_LEAVE_POLICY_IDS['bank_holiday']);
    }
}
