<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeavePolicy extends Model
{
    const DEFAULT_LEAVE_POLICY_IDS = [
        'bank_holiday' => 1,
        'holiday' => 2,
        'sick' => 3,
    ];

    public $timestamps = false;

    public $guarded = [];
}
