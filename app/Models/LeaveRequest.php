<?php

namespace App\Models;

use App\Traits\InteractsWithCycles;
use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{
    use InteractsWithCycles;

    const STATUSES = [
        'pending',
        'approved',
        'rejected',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function leaveDays()
    {
        return $this->hasMany(LeaveDay::class);
    }

    /**
     * @return string|null
     */
    public function getTypeAttribute()
    {
        return $this->leaveDays->first()->policy->name ?? null;
    }
}
