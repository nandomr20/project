<?php

use App\Models\LeavePolicy;
use App\Models\LeaveDay;
use App\Models\User;
use Illuminate\Database\Seeder;

class LeaveDaySeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        // @todo: Add leave days for each user
//        User::get()->each(function (User $user) {
            // Here $user represents an iteration of user, there it's a single user inside of a loop

            // Notice that we have two foreign keys in leave_days table  database/migrations/2020_09_12_193524_create_leave_days_table.php:24
            // We need to cover them.

            // (policy_id) Extract policy randomly
            // $randomPolicyId = ********

            // Prepare a leave day without persisting it to the database yet (we can't since we didn't cover user_id yet)
            // $leaveDays = factory(LeaveDayFactory::class, 40)->make(['policy_id' => $randomPolicyId]);

            // Assign to user through Eloquent
            // $user->leaveDays()->saveMany()

//        });
    }
}
