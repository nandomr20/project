<?php

use App\Models\LeavePolicy;
use App\Models\Company;
use Illuminate\Database\Seeder;

class LeavePolicySeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        foreach (LeavePolicy::DEFAULT_LEAVE_POLICY_IDS as $policyName => $id) {
            LeavePolicy::create(['id' => $id, 'name' => $policyName]);
        }

        if (app()->environment() !== 'production') {
            factory(LeavePolicy::class, 10)->create([
                'company_id' => Company::inRandomOrder()->first()->id
            ]);
        }
    }
}
