<?php

use App\Models\Company;
use App\Models\Cycle;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        $users = factory(User::class, 100)->make()->each(function (User $user) {
        	$user->company()->associate(Company::inRandomOrder()->first())->save();
        });
        $this->seedCycles($users);
        $supervisors = $users->splice(0, 10);

        $users->chunk(10, function (User $user) {
            $user->company()->associate(factory(Company::class)->make())->save();
        });
        $this->createSupervisors($supervisors);
    }

    /**
     * @param $users
     */
    private function seedCycles(Collection $users)
    {
        $users->each(function (User $user) {
            $user->cycles()->saveMany([
                factory(Cycle::class)->state('previous-year')->make(),
                factory(Cycle::class)->state('current-year')->make(),
            ]);
        });
    }

    /**
     * @param $users
     */
    private function createSupervisors($users): void
    {
        $iterator = 1;
        $users->each(function (User $supervisor) use ($users, &$iterator) {
            $supervisor->supervised()->attach($users->pluck('id'));
            $supervisor->cycles()->save(Cycle::find($iterator));
            $iterator++;
        });
    }
}
