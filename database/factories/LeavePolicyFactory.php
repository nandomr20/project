<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\LeavePolicy;
use Faker\Generator as Faker;

$policies = [
    'Time off in lieu',
    'Compassionate Leave',
    'Other Events',
    'Bonus Annual Holiday',
];

$factory->define(LeavePolicy::class, function (Faker $faker) use ($policies) {
    return [
        'name' => $faker->randomElement($policies),
    ];
});
