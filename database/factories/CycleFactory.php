<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cycle;
use Faker\Generator as Faker;

$factory->define(Cycle::class, function (Faker $faker) {
    return [
        'start_date' => now()->startOfYear(),
        'end_date' => now()->endOfYear(),
        'sick_days_allowance' => $faker->numberBetween(10, 15),
        'sick_days_balance' => $faker->numberBetween(0, 10),
        'holidays_days_allowance' => $faker->numberBetween(15, 25),
        'holidays_days_balance' => $faker->numberBetween(0, 15),
        'admin_adjustments_balance' => $faker->numberBetween(0, 5),
    ];
});

$factory->state(Cycle::class, 'previous-year', function (Faker $faker) {
    return ['start_date' => now()->subYears(1)->startOfYear()];
});

$factory->state(Cycle::class, 'current-year', function (Faker $faker) {
    return ['start_date' => now()->startOfYear()];
});
