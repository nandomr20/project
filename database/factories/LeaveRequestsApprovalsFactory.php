<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\LeaveRequest;
use App\Models\LeaveRequestApproval;
use Faker\Generator as Faker;

$factory->define(LeaveRequestApproval::class, function (Faker $faker) {
    return [
        'leave_request_id' => LeaveRequest::inRandomOrder()->first()->id,
        'supervisor_id'    => \DB::table('user_supervisor')->inRandomOrder()->first()->supervisor_id,
        'status'           => $faker->randomElement(LeaveRequest::STATUSES),
    ];
});
