<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\LeavePolicy;
use App\Models\LeaveDay;
use Faker\Generator as Faker;

$factory->define(LeaveDay::class, function (Faker $faker) {
    return [
        'date' => now()->subDays(mt_rand(0, 90)),
        'leave_period' => $faker->randomElement(LeaveDay::PERIODS),
    ];
});
